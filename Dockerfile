FROM python:3.6.9-slim-buster

WORKDIR /app

COPY . /app

RUN pip install -U pip && \
    pip install --no-cache-dir -r requirements.txt
