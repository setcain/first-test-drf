from django.contrib import admin
from .models import Person


class PersonAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated',)


admin.site.register(Person, PersonAdmin)
