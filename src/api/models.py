from django.db import models


class Person(models.Model):
    m = models
    name = m.CharField('Nombre', max_length=100)
    last_name = m.CharField('Apellido', max_length=100)
    created = m.DateTimeField('Creado', auto_now_add=True)
    updated = m.DateTimeField('Modificado', auto_now=True)

    class Meta:
        verbose_name = 'Persona'
        ordering = ['-created']

    def __str__(self):
        return '{0}, {1}'.format(self.last_name, self.name)
